# instagram_clon
인스타그램 클론 프로젝트

## branch 설명
* firebase_login_google : (20.02.07) 인프런강의 듣고 수강
* flutter_ui_1 : (20.02.10) 인프런강의 듣고 수강 / homepage, account 작성
* flutter_ui_2 : (20.02.13) 인프런강의 듣고 수강 / search, create 작성 + img picker 라이브러리 구현
* firebase_add_db+storage : (20.02.13) 인프런강의 듣고 수강 / db, storage - insert, select
* bora_CRM_001 : (20.02.19 ~ 02.25) user list page / create page / search filter
* bora_CRM_002 : (20.02.25) +search filter
* bora_CRM_002 : (20.02.25) list(front+delete)
* bora_CRM_003 : (20.02.25) list(update)
* bora_CRM_004 : (20.02.26) list(contacts + update img bug fix + delete storage)

## 사용  기술
* flutter
* firebase (login-google)

## 최근 작업
![image](https://user-images.githubusercontent.com/51875059/75356699-a5317300-58f3-11ea-8eac-cf2f650ba29f.png)
![image](https://user-images.githubusercontent.com/51875059/75356726-ad89ae00-58f3-11ea-9965-223280cdc180.png)
![image](https://user-images.githubusercontent.com/51875059/75356752-b4b0bc00-58f3-11ea-8910-3d6020911f5a.png)

![flutter_ui_basic_1](https://user-images.githubusercontent.com/51875059/74154541-30550c80-4c56-11ea-9590-1c8ffcab9150.gif)

### 강의 정리 내용
* [1. notion - 강의 뼈대 작성](https://www.notion.so/artlabai/200206_flutter_-ac6c8279d410410499d4691ae7fc3e0f)
* [2. notion - 로그인 UI, firebase로 google 인증](https://www.notion.so/artlabai/200206_flutter_-_-e3dc73fd61544a8192f2b3b838eae1d1)
* [3. notion - [flutterUI1] UI만들기1 (home, account)](https://www.notion.so/artlabai/200210_flutter_tab-UI-1-c75b31c1c9f74d4780beeaa5031ed942)
* [4. notion - [flutterUI1] UI만들기2 (search, create) + gallery(img picker lib)](https://www.notion.so/artlabai/200211_flutter_tab-UI-2-1e64d4f5ddd4428da116b169b80c1702)
* [5. notion - firebase storage, DB - insert,select](https://www.notion.so/artlabai/200213_flutter_fb-db-storage-insert-delete-5fdac0826f9c4d2ba7f0b3c503ff8647)
