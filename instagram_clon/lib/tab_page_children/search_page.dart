import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:instagram_clon/tab_page_children/create_page.dart';
import 'package:instagram_clon/tab_page_children/detail_page.dart';

class SearchPage extends StatefulWidget {
  FirebaseUser firebaseUser;
  SearchPage(this.firebaseUser);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CreatePage(widget.firebaseUser)));
          },
          child: Icon(Icons.mode_edit),
          elevation: 4.0,
          backgroundColor: Colors.blue
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text(
        'bora test',
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildBody() {
    return StreamBuilder(
      stream: Firestore.instance.collection('createInfo').snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshots){

        if(!snapshots.hasData){
          print('search UI - data X');
          return Text('no data');
        }else{

          print('search UI - data O');
          var dataList = snapshots.data.documents ?? [];
          return GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1.0,
                crossAxisCount: 3,
                crossAxisSpacing: 1.0,
                mainAxisSpacing: 1.0
            ),
            itemCount: dataList.length,
            itemBuilder: (context, idx) {
              return Hero(
                tag: dataList[idx]['content_img'],
                child: Material(
                  child: InkWell(
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailPage(dataList[idx])
                          ));
                    },
                    child: Image.network(
                      dataList[idx]['content_img'],
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              );
            },
          );
        }
      },
    );
  }
}