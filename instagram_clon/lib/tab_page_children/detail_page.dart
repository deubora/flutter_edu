import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  var dataObj;
  DetailPage(this.dataObj);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(
          'Detail Content',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold))),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: NetworkImage(dataObj['user_photo']??''),
                    ),
                    Padding(padding: EdgeInsets.all(8.0)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          dataObj['user_email'],
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(dataObj['user_name']),
                      ],
                    )
                  ],
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                Hero(
                    tag: dataObj['content_img'],
                    child: Image.network(dataObj['content_img'])
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                Text(dataObj['content_text'])
              ],
            ),
          ),
        ),
      ),
    );
  }
}
