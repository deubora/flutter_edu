import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AccountPage extends StatefulWidget {
  FirebaseUser firebaseUser;
  AccountPage(this.firebaseUser);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  int _docCount;
  @override
  void initState() {
    Firestore.instance.collection('createInfo').where(
      'user_email',isEqualTo: widget.firebaseUser.email
    ).getDocuments().then((snapshot){
      setState(() {
        _docCount=snapshot.documents.length;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.exit_to_app),
          onPressed: () {
            FirebaseAuth.instance.signOut();
            GoogleSignIn().signOut();
          },
        )
      ],
      title: Text(
        'bora test',
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.all(10.0)),
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        SizedBox(
                          width: 85.0,
                          height: 85.0,
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(widget
                                        .firebaseUser.photoUrl ==
                                    null
                                ? 'https://img.insight.co.kr/static/2019/05/06/700/8ungjey4n24wre99yf55.jpg'
                                : widget.firebaseUser.photoUrl),
                          ),
                        ),
                        Container(
                          width: 85.0,
                          height: 85.0,
                          alignment: Alignment.bottomRight,
                          child: Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              SizedBox(
                                  width: 28.0,
                                  height: 28.0,
                                  child: FloatingActionButton(
                                    onPressed: () {},
                                    backgroundColor: Colors.white,
                                  )),
                              SizedBox(
                                  width: 25.0,
                                  height: 25.0,
                                  child: FloatingActionButton(
                                    onPressed: () {},
                                    child: Icon(Icons.add),
                                    backgroundColor: Colors.blue,
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.all(5.0)),
                    Text(
                      widget.firebaseUser.displayName,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
                    )
                  ],
                ),
                Text(
                  '${_docCount}\n게시물',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18.0),
                ),
                Text(
                  '0\n팔로워',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18.0),
                ),
                Text(
                  '0\n팔로잉',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18.0),
                ),
              ]),
        ),
      ],
    );
  }
}
