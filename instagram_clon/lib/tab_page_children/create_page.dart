import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

class CreatePage extends StatefulWidget {
  FirebaseUser firebaseUser;
  CreatePage(this.firebaseUser);

  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  File _image;
  final TextEditingController _textController = TextEditingController();

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: _buildBody(context),
      floatingActionButton: FloatingActionButton(
        onPressed: _getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }

  Widget _buildAppBar (BuildContext context) {
    return AppBar(
      title: Text(
        'Create Content',
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.send),
          onPressed: (){
            _setData();
          },
        )
      ],
    );
  }

  Widget _buildBody (BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          _image == null
            ? Text('No image selected.')
            : Image.file(_image),
          TextField(
            controller: _textController,
            decoration: InputDecoration(hintText: '내용을 입력해주세요:)'),
          )
        ],
      ),
    );
  }

  Future _getImage() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = img;
    });
  }

  void _setData() {
    // img upload (firebase storage)
    final storageRef = FirebaseStorage().ref()
        .child('createImg').child('${DateTime.now().millisecondsSinceEpoch}.png');
    final task = storageRef.putFile(_image, StorageMetadata(contentType: 'image/png'));

    task.onComplete.then((snapshot){
      snapshot.ref.getDownloadURL().then((val){
        final imgUrl=val.toString();

        // data insert (firebase cloudStore)
        final doc = Firestore.instance.collection('createInfo').document();
        doc.setData({
          'bora_id': doc.documentID,
          'user_email': widget.firebaseUser.email,
          'user_name': widget.firebaseUser.displayName,
          'user_photo': widget.firebaseUser.photoUrl,
          'content_img': imgUrl,
          'content_text': _textController.text,
        }).then((onVal){
          Navigator.pop(context);
        });
      });
    });
  }
}
