import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  // construtor
  FirebaseUser firebaseUser;

  HomePage(this.firebaseUser);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'bora test',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
        body: _buildBody());
  }

  Widget _buildBody() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: SafeArea(
        child: SingleChildScrollView(
            child: Center(
          child: Column(
            children: <Widget>[
              Text(
                "Bora PJ에 오신걸 환영합니다.",
                style: TextStyle(fontSize: 24.0),
              ),
              Text("사진과 동영상을 보려면 팔로우 하세요."),
              Padding(
                padding: EdgeInsets.all(18.0),
              ),
              SizedBox(
                width: 280.0,
                child: Card(
                  elevation: 4.0,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      SizedBox(
                          width: 80.0,
                          height: 80.0,
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(firebaseUser.photoUrl == null
                                ? 'https://img.insight.co.kr/static/2019/05/06/700/8ungjey4n24wre99yf55.jpg'
                                : firebaseUser.photoUrl),
                          )),
//                        child: CircleAvatar(backgroundImage: NetworkImage(firebaseUser.photoUrl),)),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      Text(firebaseUser.email, style: TextStyle( fontWeight: FontWeight.bold, fontSize: 16.0)),
                      Text(firebaseUser.displayName),
                      Padding(padding: EdgeInsets.all(10.0)),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                                width: 70.0,
                                height: 70.0,
                                child: Image.network(
                                  'https://i.pinimg.com/736x/a9/d8/15/a9d815e07a496767083a315d07235cd2.jpg',
                                  fit: BoxFit.cover,
                                )),
                            Padding(padding: EdgeInsets.all(1.0)),
                            SizedBox(
                                width: 70.0,
                                height: 70.0,
                                child: Image.network(
                                  'https://i.pinimg.com/736x/a9/d8/15/a9d815e07a496767083a315d07235cd2.jpg',
                                  fit: BoxFit.cover,
                                )),
                            Padding(padding: EdgeInsets.all(1.0)),
                            SizedBox(
                                width: 70.0,
                                height: 70.0,
                                child: Image.network(
                                  'https://i.pinimg.com/736x/a9/d8/15/a9d815e07a496767083a315d07235cd2.jpg',
                                  fit: BoxFit.cover,
                                )),
                          ]),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                      ),
                      RaisedButton(
                        child: Text('팔로우'),
                        color: Colors.blue,
                        textColor: Colors.white,
                        onPressed: () {
                          print(firebaseUser.photoUrl);
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        )),
      ),
    );
  }
}
