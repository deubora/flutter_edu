import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatelessWidget {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Instagram Clon',
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold)),
          Padding(
            padding: EdgeInsets.all(30.0),
          ),
          SignInButton(
            Buttons.GoogleDark,
            onPressed: () {
              print('click :>');
              _signWithGoogle().then((user){
                print('성공 후 $user');
              }).catchError((e) => print("error ="+e.toString()));
            },
          )
        ],
      )),
    );
  }

  Future<FirebaseUser> _signWithGoogle() async {
    print('google sign in S');
    GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    FirebaseUser firebaseUser = (await _firebaseAuth.signInWithCredential(
            GoogleAuthProvider.getCredential(
                idToken: googleAuth.idToken,
                accessToken: googleAuth.accessToken)))
        .user;
    print("user info = "+firebaseUser.displayName);
    return firebaseUser;
  }
}
