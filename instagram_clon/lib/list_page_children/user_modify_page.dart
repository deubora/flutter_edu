import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:image_picker/image_picker.dart';

class UserModifyPage extends StatefulWidget {
  dynamic _dataObj;
  UserModifyPage(this._dataObj);

  @override
  _UserModifyPageState createState() => _UserModifyPageState();
}

class _UserModifyPageState extends State<UserModifyPage> {
  File _image;
  String _beforeimage;
  String _beforeimage_name;

  // text
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _memoController = TextEditingController();
  String _dropdownSeleted;
  var _radioSeleted;
  List<dynamic> _checkboxSeleted = [false, false, false];

  @override
  void initState() {
    super.initState();

    // modify data setting
    _nameController.text = widget._dataObj['user_name'];
    _phoneController.text = widget._dataObj['user_phone'];
    _memoController.text = widget._dataObj['user_memo'];
    _dropdownSeleted = widget._dataObj['user_dropdown'];
    _radioSeleted = widget._dataObj['user_radio'];
    _checkboxSeleted.addAll(widget._dataObj['user_checkbox']);
    _beforeimage = widget._dataObj['user_img'];
    _beforeimage_name = widget._dataObj['user_name'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(context),
      floatingActionButton: _buildFloating(),
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    _phoneController.dispose();
    _memoController.dispose();
    super.dispose();
  }

  Widget _buildAppBar() {
    return AppBar(
      centerTitle: true,
      actions: <Widget>[
        FlatButton(
          textColor: Colors.deepPurple,
          onPressed: _updateUser,
          color: Colors.white70,
          child: Text('저장'),
          shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
        ),
      ],
      title: Text(
        '수정',
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Theme(
        data: new ThemeData(
          primaryColor: Colors.deepPurple,
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0),
            child: _generalInputForm(),
          ),
        ));
  }

  Widget _buildFloating() {
    return FloatingActionButton(
      child: Icon(Icons.save),
      onPressed: _updateUser,
    );
  }

  Widget _generalInputForm() {
    return Column(
      children: <Widget>[
        TextField(
          onChanged: (val) {},
          controller: _nameController,
          cursorColor: Colors.deepPurple,
          decoration: InputDecoration(
            labelText: "이름",
            hintText: " 이름 입력해주세요.",
            prefixIcon: Icon(Icons.person),
          ),
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        TextField(
          onChanged: (val) {},
          controller: _phoneController,
          cursorColor: Colors.deepPurple,
          keyboardType: TextInputType.phone,
//          inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
          decoration: InputDecoration(
            labelText: "전화번호",
            hintText: " 010-0000-0000",
            prefixIcon: Icon(Icons.phone),
          ),
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        TextField(
          onChanged: (val) {},
          controller: _memoController,
          cursorColor: Colors.deepPurple,
          decoration: InputDecoration(
            labelText: "특징",
            hintText: " 특징 입력해주세요.",
            prefixIcon: Icon(Icons.featured_play_list),
          ),
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        Text(
          '경로 :',
          style: new TextStyle(
            fontSize: 15.0,
          ),
        ),
        OutlineDropdownButton(
          inputDecoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(8.0)),
          items: <String>['훈재', '보라', '해일', '테리'].map((String val) {
            return DropdownMenuItem<String>(
              value: val,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(val),
              ),
            );
          }).toList(),
          value: _dropdownSeleted,
          onChanged: (val) {
            setState(() {
              _dropdownSeleted = val;
            });
          },
          hint: Text('경로를 선택해주세요.'),
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(Icons.location_searching),
            Text('경로'),
            Container(
              width: 300,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0)),
              child: DropdownButton<String>(
                items: <String>['훈재', '보라', '해일', '테리'].map((String val) {
                  return DropdownMenuItem<String>(
                    value: val,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(val),
                    ),
                  );
                }).toList(),
                hint: Text('선택해주세요.'),
                value: _dropdownSeleted,
                onChanged: (val) {
                  setState(() {
                    _dropdownSeleted = val;
                  });
                },
                underline: SizedBox(),
              ),
            ),
          ],
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        Text('Radio:'),
        Row(
          children: <Widget>[
            Radio(
                activeColor: Colors.deepPurple,
                value: 1,
                groupValue: _radioSeleted,
                onChanged: (val) => setState(() => _radioSeleted = val)),
            Text('아니 이렇게?'),
            Radio(
                activeColor: Colors.deepPurple,
                value: 2,
                groupValue: _radioSeleted,
                onChanged: (val) => setState(() => _radioSeleted = val)),
            Text('하나하나'),
            Radio(
                activeColor: Colors.deepPurple,
                value: 3,
                groupValue: _radioSeleted,
                onChanged: (val) => setState(() => _radioSeleted = val)),
            Text('만들어야된다고?'),
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
              value: _checkboxSeleted[0],
              onChanged: (val) => setState(() => _checkboxSeleted[0] = val),
              activeColor: Colors.deepPurple,
            ),
            Text('우와'),
            Checkbox(
              value: _checkboxSeleted[1],
              onChanged: (val) => setState(() => _checkboxSeleted[1] = val),
              activeColor: Colors.deepPurple,
            ),
            Text('되게'),
            Checkbox(
              value: _checkboxSeleted[2],
              onChanged: (val) => setState(() => _checkboxSeleted[2] = val),
              activeColor: Colors.deepPurple,
            ),
            Text('비효율적'),
          ],
        ),
        imgForm()
      ],
    );
  }

  Widget imgForm() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: _image != null
              ? Image.file(_image)
              : _beforeimage != null
                ? Image.network(_beforeimage,fit: BoxFit.fill,)
                : Center(child: Text('no image')),
          width: 200,
          height: 200,
          color: Colors.white70,
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        Column(
          children: <Widget>[
            RaisedButton(
              child: _image == null && widget._dataObj['user_img'] == null
                  ? Text('이미지 추가') : Text('이미지 변경'),
              color: _image == null && widget._dataObj['user_img'] == null
                  ? Colors.amberAccent : Colors.deepPurple,
              textColor: Colors.white,
              onPressed: _getImage,
            ),
            _image == null && widget._dataObj['user_img'] == null
                ? Container(width: 10) //dummy
                : RaisedButton(
                    child: Text('이미지 삭제'),
                    onPressed: () {
                      setState(() {
                        _image = null;
                      });
                    },
                  )
          ],
        ),
      ],
    );
  }

  Future _getImage() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = img;
    });
  }

  void _updateUser() {
    print('update start');
    if (_image != null) {
      var imgName = '${DateTime.now().millisecondsSinceEpoch}.png';

      final storageRef = FirebaseStorage().ref().child('userList').child(imgName);
      final task = storageRef.putFile(_image);

      task.onComplete.then((snapshot) {
        snapshot.ref.getDownloadURL().then((val) {
          final imgUrl = val.toString();

          // update code
          final storeDoc = Firestore.instance.collection('userList').document(widget._dataObj.documentID);
          storeDoc.updateData({
            'user_name': _nameController.text,
            'user_phone': _phoneController.text,
            'user_memo': _memoController.text,
            'user_dropdown': _dropdownSeleted,
            'user_radio': _radioSeleted,
            'user_checkbox': _checkboxSeleted,
            'user_img' : imgUrl,
            'user_img_name': imgName,
          }).catchError((e)=>print(e)).then((v)=>Navigator.pop(context));
        });
      });
    }else{
      if (_beforeimage != null){
        // update code
        final storeDoc = Firestore.instance.collection('userList').document(widget._dataObj.documentID);
        storeDoc.updateData({
          'user_name': _nameController.text,
          'user_phone': _phoneController.text,
          'user_memo': _memoController.text,
          'user_dropdown': _dropdownSeleted,
          'user_radio': _radioSeleted,
          'user_checkbox': _checkboxSeleted,
        }).catchError((e)=>print(e)).then((v)=>Navigator.pop(context));
      }else{
        // delete file
        if(_beforeimage_name != null){
          final storageRef = FirebaseStorage().ref().child('userList/'+_beforeimage_name);
          storageRef.delete().then((v)=>print('delete file = '+_beforeimage_name)).catchError((e)=>print(e));
        }

        // update code
        final storeDoc = Firestore.instance.collection('userList').document(widget._dataObj.documentID);
        storeDoc.updateData({
          'user_name': _nameController.text,
          'user_phone': _phoneController.text,
          'user_memo': _memoController.text,
          'user_dropdown': _dropdownSeleted,
          'user_radio': _radioSeleted,
          'user_checkbox': _checkboxSeleted,
          'user_img' : null,
          'user_img_name': null,
        }).catchError((e)=>print(e)).then((v)=>Navigator.pop(context));
      }
    }
  }
}
