import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class ContactsServiceListPage extends StatefulWidget {
  FirebaseUser firebaseUser;

  ContactsServiceListPage(this.firebaseUser);

  @override
  _ContactsServiceListPageState createState() =>
      _ContactsServiceListPageState();
}

class _ContactsServiceListPageState extends State<ContactsServiceListPage> {
  EasyPermissionValidator permissionValidator;

  // contacts
  List<Contact> _contacts = new List<Contact>();
  List<ContactObj> _uiContacts = List<ContactObj>();
  List<ContactObj> _allContacts = List<ContactObj>();
  int _selectedCnt = 0;
  int compliteCnt = 0;

  @override
  void initState() {
    super.initState();
    permissionValidator =
        EasyPermissionValidator(context: context, appName: 'Bora');
    refreshContacts();
  }

  void refreshContacts() async {
    var result = await permissionValidator.contacts();
    if (result) {
      await ContactsService.getContacts().then((val) => _populateContacts(val));
    }
  }

  void _populateContacts(Iterable<Contact> contacts) {
    _contacts = contacts.where((item) => item.displayName != null).toList();
    _contacts.sort((a, b) => a.displayName.compareTo(b.displayName));
    _allContacts =
        _contacts.map((contact) => ContactObj(contact: contact)).toList();

    setState(() {
      _uiContacts = _allContacts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('${_selectedCnt ?? 0}명 추가하기'),
      actions: <Widget>[
        FlatButton(
          textColor: Colors.deepPurple,
          color: Colors.white70,
          onPressed: _selectedCnt > 0 ? _onSubmit : null,
          child: Text('저장'),
          shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
        ),
      ],
    );
  }

  void _onSubmit() {
    _uiContacts = _allContacts.where((obj) => obj.isChecked == true).toList();

    // bulk insert
    _uiContacts.forEach((obj) {
      if (obj.contact.avatar.toString() != '[]') {
        var imgName = '${DateTime.now().millisecondsSinceEpoch}.png';

        final storageRef = FirebaseStorage().ref().child('userList').child(imgName);
        final task = storageRef.putData(obj.contact.avatar);

        task.onComplete.then((snapshot) {
          snapshot.ref.getDownloadURL().then((val) {
            // insert code
            final storeDoc = Firestore.instance.collection('userList').document();
            storeDoc.setData({
              'id': storeDoc.documentID,
              'user_name': obj.contact.displayName,
              'user_phone': obj.contact.phones.toList()[0].value,
              'user_momo': '[연락처 자동 추가]',
              'user_dropdown': null,
              'user_radio': null,
              'user_checkbox': [false, false, false],
              'user_img': val.toString(),
              'user_img_name': imgName,
              'author_email': widget.firebaseUser.email,
              'author_name': widget.firebaseUser.displayName,
              'author_photo': widget.firebaseUser.photoUrl,
            }).then((onVal) {
              compliteCnt++;
              if (compliteCnt == _selectedCnt) {
                Navigator.pop(context);
              }
            }).catchError((e) => print(e));
          });
        });
      } else {
        // insert code
        final storeDoc = Firestore.instance.collection('userList').document();
        storeDoc.setData({
          'id': storeDoc.documentID,
          'user_name': obj.contact.displayName,
          'user_phone': obj.contact.phones.toList()[0].value,
          'user_momo': '[연락처 자동 추가]',
          'user_dropdown': null,
          'user_radio': null,
          'user_checkbox': [false, false, false],
          'user_img': null,
          'author_email': widget.firebaseUser.email,
          'author_name': widget.firebaseUser.displayName,
          'author_photo': widget.firebaseUser.photoUrl,
        }).then((onVal) {
          compliteCnt++;
          if (compliteCnt == _selectedCnt) {
            Navigator.pop(context);
          }
        }).catchError((e) => print(e));
      }
    });
  }

  void insertCode2(var imgUrl, var obj) {
    print(imgUrl);
    // insert code
    final storeDoc = Firestore.instance.collection('userList').document();
    storeDoc.setData({
      'id': storeDoc.documentID,
      'user_name': obj.contact.displayName,
      'user_phone': obj.contact.phones.toList()[0].value,
      'user_momo': '[연락처 자동 추가]',
      'user_img': imgUrl == null ? null : imgUrl,
      'user_dropdown': null,
      'user_radio': null,
      'user_checkbox': null,
      'author_email': widget.firebaseUser.email,
      'author_name': widget.firebaseUser.displayName,
      'author_photo': widget.firebaseUser.photoUrl,
    }).whenComplete(() {
      print('완료! ===================> ${compliteCnt}');
      compliteCnt++;
      if (compliteCnt == _selectedCnt) {
        Navigator.pop(context);
      }
    }).catchError((e) => print('에러?>>>>>>>>>'));
  }

  Widget _buildBody() {
    return Container(
      child: ListView.builder(
        itemCount: _uiContacts?.length,
        itemBuilder: (BuildContext context, int index) {
          ContactObj _contact = _uiContacts[index];
          return _buildListTile(_contact, _contact.contact.phones.toList());
        },
      ),
    );
  }

  Widget _buildListTile(ContactObj obj, var phonesList) {
    return ListTile(
      leading: (obj.contact.avatar.toString() != '[]')
          ? CircleAvatar(backgroundImage: MemoryImage(obj.contact.avatar))
          : CircleAvatar(backgroundColor: Colors.deepPurple),
      title: Text(obj.contact.displayName ?? ""),
      subtitle: phonesList.length >= 1 && phonesList[0]?.value != null
          ? Text(phonesList[0].value)
          : Text(''),
      trailing: Checkbox(
        activeColor: Colors.deepPurple,
        value: obj.isChecked,
        onChanged: (val) => setState(() {
          obj.isChecked = val;
          val ? _selectedCnt++ : _selectedCnt--;
        }),
      ),
    );
  }
}

class ContactObj {
  final Contact contact;
  bool isChecked;

  ContactObj({
    this.contact,
    this.isChecked = false,
  });
}
