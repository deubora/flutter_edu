import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:contact_picker/contact_picker.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:instagram_clon/list_page_children/contacts/contacts_service_list.dart';
import 'package:instagram_clon/list_page_children/user_create_page.dart';
import 'package:instagram_clon/list_page_children/user_modify_page.dart';


class ContactObj {
  final Contact contact;
  bool isChecked;

  ContactObj({
    this.contact,
    this.isChecked = false,
  });
}

// -------------------------------------------------------
class UserListPage extends StatefulWidget {
  FirebaseUser firebaseUser;
  UserListPage(this.firebaseUser);

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  TextEditingController _searchTextController = TextEditingController();

  // contacts
  List<Contact> _contacts = new List<Contact>();
  List<ContactObj> _uiContacts = List<ContactObj>();
  List<ContactObj> _allContacts = List<ContactObj>();

  var _dataList = [];
  var _duplicateDataList = [];

  @override
  void initState() {
    super.initState();
    Firestore.instance.collection('userList').snapshots().listen((data) {
      setState(() {
        _dataList.clear();
        _duplicateDataList.clear();
        _dataList.addAll(data?.documents ?? []);
        _duplicateDataList.addAll(_dataList);
      });
    });
  }

  void refreshContacts() async {
    var contacts = await ContactsService.getContacts();
    _populateContacts(contacts);
  }
  void _populateContacts(Iterable<Contact> contacts) {
    _contacts = contacts.where((item) => item.displayName != null).toList();
    _contacts.sort((a, b) => a.displayName.compareTo(b.displayName));

    _allContacts = _contacts.map((contact) => ContactObj(contact: contact)).toList();
    setState(() {
      _uiContacts = _allContacts;
    });
  }

  @override
  void dispose() {
    _searchTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      centerTitle: true,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.person_add),
          onPressed: clickAdd,
        )
      ],
      title: Text(
        '고객 목록',
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
    );
  }

  void clickAdd() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: ModalDrawerHandle(
                  handleColor: Colors.deepPurple,
                ),
              ),
              ListTile(
                leading: Icon(Icons.add_circle),
                title: Text('직접 입력하기'),
                onTap: moveCreatePage,
              ),
              ListTile(
                leading: Icon(Icons.phone_forwarded),
                title: Text('연락처 가져오기'),
                onTap: moveContactsPage,
              ),
            ],
          );
        });
  }

  void moveCreatePage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => UserCreatePage(widget.firebaseUser)));
  }
  void moveContactsPage() {
    print('bora');
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ContactsServiceListPage(widget.firebaseUser)));
  }

  Widget _buildBody() {
    return Theme(
      data: new ThemeData(
        primaryColor: Colors.deepPurple,
      ),
      child: Column(
        children: <Widget>[
          Padding(padding: const EdgeInsets.all(10.0), child: _searchBox()),
          Expanded(
              child: _dataList.length > 0
                  ? _listView()
                  : Center(
                      child: Text('no data'),
                    )),
        ],
      ),
    );
  }

  Widget _searchBox() {
    return TextField(
      onChanged: (val) => filterSearchResults(val),
      controller: _searchTextController,
      cursorColor: Colors.deepPurple,
      decoration: InputDecoration(
          labelText: "Search",
          hintText: "이름으로 검색",
          prefixIcon: Icon(Icons.search),
          suffixIcon: _searchTextController.text.isEmpty
              ? null
              : IconButton(
                  icon: Icon(Icons.clear),
                  onPressed: () {
                    setState(() {
                      _searchTextController.clear();
                      _dataList.clear();
                      _dataList.addAll(_duplicateDataList);
                    });
                  },
                ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0)))),
    );
  }

  void filterSearchResults(String query) {
    if (query.isNotEmpty) {
      var dummyListData = [];
      dummyListData.addAll(_duplicateDataList);
      dummyListData.removeWhere((obj) => !obj['user_name'].contains(query));

      setState(() {
        _dataList.clear();
        _dataList.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        _dataList.clear();
        _dataList.addAll(_duplicateDataList);
      });
    }
  }

  Widget _listView() {
    return ListView.builder(
        itemCount: _dataList.length,
        itemBuilder: (context, idx) {
          return ListTile(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UserModifyPage(_dataList[idx])
                  ));
            },
            title: Text('${_dataList[idx]['user_name']}'),
            subtitle: Text('${_dataList[idx]['user_phone']}'),
            leading: CircleAvatar(
                backgroundColor: Colors.white,
                backgroundImage: _dataList[idx]['user_img'] == null
                    ? NetworkImage(
                        'https://cdn.pixabay.com/photo/2017/11/10/05/48/user-2935527_960_720.png')
                    : NetworkImage(_dataList[idx]['user_img'])),
            trailing: IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                var _beforeimage_name = _dataList[idx]['user_img_name'];
                print(1);
                // delete file
                if(_beforeimage_name != null){
                  final storageRef = FirebaseStorage().ref().child('userList/'+_beforeimage_name);
                  storageRef.delete().then((v)=>print('3 delete file = '+_beforeimage_name)).catchError((e)=>print(e));
                }
                print(2);
                // delete data
                var docId = _dataList[idx].documentID;
                Firestore.instance
                    .collection('userList')
                    .document(docId)
                    .delete()
                    .whenComplete(() {
                  setState(() {
                    _dataList.removeWhere((obj) => obj['id'] == docId);
                    _duplicateDataList.removeWhere((obj) => obj['id'] == docId);
                  });
                  print('delete complite');
                }).catchError((e) => print(e));
              },
            ),
          );
        });
  }
}


