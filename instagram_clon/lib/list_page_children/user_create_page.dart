import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:image_picker/image_picker.dart';

class UserCreatePage extends StatefulWidget {
  FirebaseUser firebaseUser;
  UserCreatePage(this.firebaseUser);

  @override
  _UserCreatePageState createState() => _UserCreatePageState();
}

class _UserCreatePageState extends State<UserCreatePage> {
  File _image;

  // text
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _memoController = TextEditingController();
  String _dropdownSeleted;
  var _radioSeleted;
  var _checkboxSeleted = [false, false, false];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(context),
      floatingActionButton: _buildFloating(),
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    _phoneController.dispose();
    _memoController.dispose();
    super.dispose();
  }

  Widget _buildAppBar() {
    return AppBar(
      centerTitle: true,
      actions: <Widget>[
        FlatButton(
          textColor: Colors.deepPurple,
          onPressed: _insertUser,
          color: Colors.white70,
          child: Text('저장'),
          shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
        ),
      ],
      title: Text(
        '추가',
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Theme(
        data: new ThemeData(
          primaryColor: Colors.deepPurple,
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0),
            child: _generalInputForm(),
          ),
        ));
  }

  Widget _buildFloating() {
    return FloatingActionButton(
      child: Icon(Icons.save),
      onPressed: _insertUser,
    );
  }

  Widget _generalInputForm() {
    return Column(
      children: <Widget>[
        TextField(
          onChanged: (val) {},
          controller: _nameController,
          cursorColor: Colors.deepPurple,
          decoration: InputDecoration(
            labelText: "이름",
            hintText: " 이름 입력해주세요.",
            prefixIcon: Icon(Icons.person),
          ),
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        TextField(
          onChanged: (val) {},
          controller: _phoneController,
          cursorColor: Colors.deepPurple,
          keyboardType: TextInputType.phone,
//          inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
          decoration: InputDecoration(
            labelText: "전화번호",
            hintText: " 010-0000-0000",
            prefixIcon: Icon(Icons.phone),
          ),
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        TextField(
          onChanged: (val) {},
          controller: _memoController,
          cursorColor: Colors.deepPurple,
          decoration: InputDecoration(
            labelText: "특징",
            hintText: " 특징 입력해주세요.",
            prefixIcon: Icon(Icons.featured_play_list),
          ),
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        Text(
          '경로 :',
          style: new TextStyle(
            fontSize: 15.0,
          ),
        ),
        OutlineDropdownButton(
          inputDecoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(8.0)),
          items: <String>['훈재', '보라', '해일', '테리'].map((String val) {
            return DropdownMenuItem<String>(
              value: val,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(val),
              ),
            );
          }).toList(),
          value: _dropdownSeleted,
          onChanged: (val) {
            setState(() {
              _dropdownSeleted = val;
            });
          },
          hint: Text('경로를 선택해주세요.'),
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(Icons.location_searching),
            Text('경로'),
            Container(
              width: 300,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0)),
              child: DropdownButton<String>(
                items: <String>['훈재', '보라', '해일', '테리'].map((String val) {
                  return DropdownMenuItem<String>(
                    value: val,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(val),
                    ),
                  );
                }).toList(),
                hint: Text('선택해주세요.'),
                value: _dropdownSeleted,
                onChanged: (val) {
                  setState(() {
                    _dropdownSeleted = val;
                  });
                },
                underline: SizedBox(),
              ),
            ),
          ],
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        Text('Radio:'),
        Row(
          children: <Widget>[
            Radio(
                activeColor: Colors.deepPurple,
                value: 1,
                groupValue: _radioSeleted,
                onChanged: (val) => setState(() => _radioSeleted = val)),
            Text('아니 이렇게?'),
            Radio(
                activeColor: Colors.deepPurple,
                value: 2,
                groupValue: _radioSeleted,
                onChanged: (val) => setState(() => _radioSeleted = val)),
            Text('하나하나'),
            Radio(
                activeColor: Colors.deepPurple,
                value: 3,
                groupValue: _radioSeleted,
                onChanged: (val) => setState(() => _radioSeleted = val)),
            Text('만들어야된다고?'),
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
              value: _checkboxSeleted[0],
              onChanged: (val) => setState(() => _checkboxSeleted[0] = val),
              activeColor: Colors.deepPurple,
            ),
            Text('우와'),
            Checkbox(
              value: _checkboxSeleted[1],
              onChanged: (val) => setState(() => _checkboxSeleted[1] = val),
              activeColor: Colors.deepPurple,
            ),
            Text('되게'),
            Checkbox(
              value: _checkboxSeleted[2],
              onChanged: (val) => setState(() => _checkboxSeleted[2] = val),
              activeColor: Colors.deepPurple,
            ),
            Text('비효율적'),
          ],
        ),
        imgForm1()
      ],
    );
  }

  Widget imgForm1() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: _image == null
              ? Center(child: Text('no image'))
              : Image.file(_image),
          width: 200,
          height: 200,
          color: Colors.white70,
        ),
        Padding(padding: EdgeInsets.all(8.0)),
        Column(
          children: <Widget>[
            RaisedButton(
              child: _image == null ? Text('이미지 추가') : Text('이미지 변경'),
              color: _image == null ? Colors.amberAccent : Colors.deepPurple,
              textColor: Colors.white,
              onPressed: _getImage,
            ),
            _image == null
                ? Container(width: 10) //dummy
                : RaisedButton(
                    child: Text('이미지 삭제'),
                    onPressed: () {
                      setState(() {
                        _image = null;
                      });
                    },
                  )
          ],
        ),
      ],
    );
  }

  Future _getImage() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = img;
    });
  }

  void _insertUser() {
    print('insert start');

    // img 있으면 insert
    if (_image != null) {
      var imgName = '${DateTime.now().millisecondsSinceEpoch}.png';
      final storageRef = FirebaseStorage()
          .ref()
          .child('userList')
          .child(imgName);
      final task = storageRef.putFile(_image);
      task.onComplete.then((snapshot) {
        snapshot.ref.getDownloadURL().then((val) {
          final imgUrl = val.toString();

          // insert code
          final storeDoc = Firestore.instance.collection('userList').document();
          storeDoc.setData({
            'id': storeDoc.documentID,
            'user_name': _nameController.text,
            'user_phone': _phoneController.text,
            'user_memo': _memoController.text,
            'user_dropdown': _dropdownSeleted,
            'user_radio': _radioSeleted,
            'user_checkbox': _checkboxSeleted,
            'user_img' : imgUrl,
            'user_img_name' : imgName,
            'author_email': widget.firebaseUser.email,
            'author_name': widget.firebaseUser.displayName,
            'author_photo': widget.firebaseUser.photoUrl,
          }).then((onval) => Navigator.pop(context));
        });
      });
    } else {
      // insert code
      final storeDoc = Firestore.instance.collection('userList').document();
      storeDoc.setData({
        'id': storeDoc.documentID,
        'user_name': _nameController.text,
        'user_phone': _phoneController.text,
        'user_memo': _memoController.text,
        'user_dropdown': _dropdownSeleted,
        'user_radio': _radioSeleted,
        'user_checkbox': _checkboxSeleted,
        'user_img' : null,
        'author_email': widget.firebaseUser.email,
        'author_name': widget.firebaseUser.displayName,
        'author_photo': widget.firebaseUser.photoUrl,
      }).then((onval){
        Navigator.pop(context);
      });
    }
  }

  // -----------------------------------
  Widget imgForm2() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Center(
              child: Column(
            children: <Widget>[
              Text('no image'),
              RaisedButton(
                child: Text('이미지 선택'),
                color: Colors.amberAccent,
                textColor: Colors.white,
                onPressed: () {},
              ),
            ],
          )),
          width: 200,
          height: 200,
          color: Colors.white70,
        ),
      ],
    );
  }

  Widget _inputForm2() {
    return Column(
      children: <Widget>[
        ListTile(
            leading: Icon(Icons.person),
            title: TextField(
              decoration: InputDecoration(
                hintText: '이름',
              ),
            ))
      ],
    );
  }
}
