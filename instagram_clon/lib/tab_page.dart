import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:instagram_clon/tab_page_children/account_page.dart';
import 'package:instagram_clon/tab_page_children/home_page.dart';
import 'package:instagram_clon/tab_page_children/search_page.dart';
import 'package:instagram_clon/list_page_children/user_list_page.dart';

class TabPage extends StatefulWidget {
  // constructor
  FirebaseUser firebaseUser;
  TabPage(this.firebaseUser);

  @override
  _TabPageState createState() => _TabPageState();
}

class _TabPageState extends State<TabPage> {
  int _selectedIdx = 0;
  List _pages;

  @override
  void initState() {
    // widget data를 받아오려하니깐 init stat로 받아와야함
    super.initState();
    _pages = [
      HomePage(widget.firebaseUser),
      UserListPage(widget.firebaseUser),
      SearchPage(widget.firebaseUser),
      AccountPage(widget.firebaseUser)
    ];
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _pages[_selectedIdx],
      ),
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: Colors.black,
        onTap: (idx){
          setState(() {_selectedIdx=idx;});
          },
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIdx,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
          BottomNavigationBarItem(icon: Icon(Icons.accessibility_new), title: Text('list')),
          BottomNavigationBarItem(icon: Icon(Icons.search), title: Text('Search')),
          BottomNavigationBarItem(icon: Icon(Icons.account_circle), title: Text('Account')),
        ],
      ),
    );
  }
}
