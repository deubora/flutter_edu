# [Bora] Flutter Edu #

보라 개인 공부 flutter와 firebase 등 인프런 강의와 함께 수강하면서  
노션 정리와 branch 별로 프로젝트 소스코드 백업등 같이 진행

### Project 구조 ###

* instagram_clon : 인프런 강의 위주로 진행 

### 참고 자료 ###

* 인프런 강의 : https://www.inflearn.com/course/flutter-%EC%9E%85%EB%AC%B8/dashboard
* 노션 정리 : https://www.notion.so/2-flutter-616d734f118141f18005759564c21a05
* flutter 공식 문서 : https://flutter.dev/docs
* flutter 제공 위젯 찾기 : https://flutterstudio.app/